package com.schoolmanagement.configuration;

/**
 * Created by Manas on 6/21/2016.
 */
import com.fasterxml.jackson.annotation.JsonProperty;
import io.dropwizard.Configuration;
import io.dropwizard.db.DataSourceFactory;

import javax.validation.Valid;


public class RegistrationConfiguration extends Configuration {

    @JsonProperty
    private int id;
    @JsonProperty
    private String name;
    @JsonProperty
    private String emailId;

    public int getId() {
        return id;
    }

    public String getName() {
        return name;
    }
    public String getEmailId() {
        return emailId;
    }

    public void setId(int id) {
        this.id = id;
    }

    public void setName(String name) {
        this.name = name;
    }

    public void setEmailId(String emailId) {
        this.emailId = emailId;
    }
    @Valid
    @JsonProperty
    private DataSourceFactory database = new DataSourceFactory();

    public DataSourceFactory getDataSourceFactory() {
        return database;
    }

}
