package com.schoolmanagement.model;

/**
 * Created by Manas on 6/21/2016.
 */
public class Registration {

    private final int id;
    private final String name;
    private final String emailId;

    public Registration(){
        this.id = 0;
        this.name = null;
        this.emailId = null;
    }

    public Registration(int id, String name, String emailId){
        this.id = id;
        this.name = name;
        this.emailId = emailId;

    }

    public int getId() {
        return id;
    }
    public String getName() {
        return name;
    }

    public String getEmailId() {
        return emailId;
    }


}
