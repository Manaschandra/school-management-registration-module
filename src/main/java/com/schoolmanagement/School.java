package com.schoolmanagement;

import com.schoolmanagement.configuration.RegistrationConfiguration;
import com.schoolmanagement.resources.RegistrationResource;
import io.dropwizard.Application;
import io.dropwizard.jdbi.DBIFactory;
import io.dropwizard.setup.Bootstrap;
import io.dropwizard.setup.Environment;
import org.skife.jdbi.v2.DBI;

/**
 * Created by Manas on 6/21/2016.
 */
public class School extends Application<RegistrationConfiguration> {

    public static void main(String[] args) throws Exception {
        new School().run(args);
    }


    @Override
    public void initialize(Bootstrap<RegistrationConfiguration> bootstrap) {
        // nothing to do yet
    }

    @Override
    public void run(RegistrationConfiguration configuration,
                    Environment environment) throws Exception {
        final DBIFactory factory = new DBIFactory();
        final DBI jdbi = factory.build(environment, configuration.getDataSourceFactory(),"mysql");
// Add the resource to the environment

        environment.jersey().register(new RegistrationResource(jdbi));
        System.out.println(configuration.getName());
        System.out.println(configuration.getEmailId());
    }
}
