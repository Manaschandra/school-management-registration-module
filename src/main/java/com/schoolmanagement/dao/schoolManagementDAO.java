package com.schoolmanagement.dao;

import com.schoolmanagement.dao.mappers.RegistrationMapper;
import com.schoolmanagement.model.Registration;
import org.skife.jdbi.v2.sqlobject.Bind;
import org.skife.jdbi.v2.sqlobject.GetGeneratedKeys;
import org.skife.jdbi.v2.sqlobject.SqlQuery;
import org.skife.jdbi.v2.sqlobject.SqlUpdate;
import org.skife.jdbi.v2.sqlobject.customizers.Mapper;

/**
 * Created by Manas on 6/21/2016.
 */


public interface schoolManagementDAO {
    @Mapper(RegistrationMapper.class)
    @SqlQuery("select * from students where id = :id")
    Registration studentByName(@Bind("id") int id);

    @GetGeneratedKeys
    @SqlUpdate("insert into students (id, name, emailid) values (:id, :name, :emailid)")
    int createStudent(@Bind("id") int id, @Bind("name") String name,@Bind("emailid") String emailid);

    @SqlUpdate("update students set id = :id, name = :name, emailid = :emailid where id = :id")
    void updateStudent(@Bind("id") int id, @Bind("name") String name, @Bind("emailid") String emailid);

    @SqlUpdate("delete from students where id = :id")
    void deleteStudent(@Bind("id") int id);

}
