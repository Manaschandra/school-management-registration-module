package com.schoolmanagement.dao.mappers;

import com.schoolmanagement.model.Registration;
import org.skife.jdbi.v2.StatementContext;
import org.skife.jdbi.v2.tweak.ResultSetMapper;

import java.sql.ResultSet;
import java.sql.SQLException;

/**
 * Created by Manas on 6/21/2016.
 */
public class RegistrationMapper implements ResultSetMapper<Registration> {

    public Registration map(int index, ResultSet r, StatementContext ctx)
            throws SQLException {
        return new Registration(r.getInt("id") ,r.getString("name"), r.getString("emailId"));
    }
}
