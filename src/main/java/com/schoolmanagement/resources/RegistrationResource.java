package com.schoolmanagement.resources;

/**
 * Created by Manas on 6/21/2016.
 */

import com.schoolmanagement.dao.schoolManagementDAO;
import com.schoolmanagement.model.Registration;
import org.skife.jdbi.v2.DBI;

import javax.ws.rs.*;
import javax.ws.rs.core.*;
import java.net.URI;
import java.net.URISyntaxException;

@Path("/registration")
@Produces(MediaType.APPLICATION_JSON)
public class RegistrationResource {
    private final schoolManagementDAO Dao;

    public RegistrationResource(DBI jdbi) {
        Dao = jdbi.onDemand(schoolManagementDAO.class);
    }

    @GET
    @Path("/{id}")
    @Produces(MediaType.APPLICATION_JSON)
    public Response getstudent(@PathParam("id") int id) {
// retrieve information about the contact with the
        Registration registration = Dao.studentByName(id);
        return Response.ok(registration).build();
    }

    @POST
    @Consumes(MediaType.APPLICATION_JSON)
    @Produces(MediaType.APPLICATION_JSON)
    public Response createContact(Registration registration) throws
            URISyntaxException {
// store the new contact
        int newStudentId = Dao.createStudent(registration.getId(),registration.getName(),registration.getEmailId());

        return Response.created(new URI(String.valueOf(newStudentId))).build();
    }

    @DELETE
    @Path("/{id}")
    public Response deleteStudent(@PathParam("id") int id) {
// delete the contact with the provided id
        Dao.deleteStudent(id);
        return Response.noContent().build();
    }

    @PUT
    @Path("/{id}")
    @Consumes(MediaType.APPLICATION_JSON)
    public Response updateContact(@PathParam("id") int id, Registration registration) {
// update the contact with the provided ID
        Dao.updateStudent(id, registration.getName(),registration.getEmailId());
        return Response.ok(new Registration(id, registration.getName(), registration.getEmailId())).build();
    }
}
