# School Management - Registration module

This module is for registering students into school. Dropwizard was used for restful web services. This module uses mySql and it is required to install mySql and have a database named schoolmanagement with a table students in it. 

## Execution

* Start the mySql server.
* Next, pull the project into your local repo.
* In your terminal, go the root of the project and type mvn clean package.
* After that, type java -jar target/schoolmanagement-1.0-SNAPSHOT.jar server config.yaml to start the rest server.
* Use postman for accessing web services. 
* Url used: http://localhost:8080/registration. Depending on the services needed, an user can use http methods(POST, GET, POST, DELETE).

## Note
This project's scope was big and this is only one module of the project. Other modules, java frameworks and front end part will be implemented in future.